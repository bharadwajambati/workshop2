import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LineComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
