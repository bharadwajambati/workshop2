import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-left-side',
  templateUrl: './left-side.component.html',
  styleUrls: ['./left-side.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LeftSideComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
