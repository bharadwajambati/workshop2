import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-navi',
  templateUrl: './navi.component.html',
  styleUrls: ['./navi.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NaviComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
