import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { NaviComponent } from './navi/navi.component';
import { LineComponent } from './line/line.component';
import { LeftSideComponent } from './left-side/left-side.component';


@NgModule({
  declarations: [
    AppComponent,
    NaviComponent,
    LineComponent,
    LeftSideComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
